from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landingPage, profile
from .models import Status
from .forms import StatusForm

#functional test
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Lab6UnitTest(TestCase):
	"""docstring for Lab6UnitTest"""
	def test_lab6_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_lab6_use_status(self):
		found = resolve('/')
		self.assertEqual(found.func, landingPage)

	def test_lab6_has_this_text(self):
		response = Client().get('')
		string = response.content.decode('utf8')
		self.assertIn('Hello, Apa Kabar?', string)

	def test_lab6_form_validation_for_blank_item(self):
		form = StatusForm(data={'status': ''})
		self.assertFalse(form.is_valid())

	def test_lab6_post_success_and_render_the_result(self):
		test = ''
		response_post = Client().post('/', {'status': test})
		self.assertEqual(response_post.status_code, 200)
		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


	def test_lab6_post_error_and_render_the_result(self):
		test = 'yoBisayo'
		response_post = Client().post('/', {'status': test})
		self.assertEqual(response_post.status_code, 200)
		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertNotIn('yoSemangatyo', html_response)

	def test_lab6_profile_has_name(self):
		response = Client().get('/profile')
		string = response.content.decode('utf8')
		self.assertIn('Nama: Siti Nurfitriyah', string)

	def test_lab6_profile_has_dateofbirth(self):
		response = Client().get('/profile')
		string = response.content.decode('utf8')
		self.assertIn('TTL: 12 November 1998', string)

	def test_lab6_profile_has_Hobby(self):
		response = Client().get('/profile')
		string = response.content.decode('utf8')
		self.assertIn('Harapan: dimudakan dalam mengerjakan soal ppw :)', string)


class Lab7FunctionalTest(TestCase):

    def setUp(self):
    	chrome_options = Options()
    	chrome_options.add_argument('--dns-prefetch-disable')
    	chrome_options.add_argument('--no-sandbox')        
    	chrome_options.add_argument('--headless')
    	chrome_options.add_argument('disable-gpu')
    	self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)


    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        message = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        time.sleep(4)
        message.send_keys('Coba Coba')
        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(4)
        #check status on page
        self.assertIn('Coba Coba', selenium.page_source)

    # def test_web_background_is_gradient(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('https://-a.herokuapp.com')
    #     body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
    #     self.assertEqual(body, "rgba(237, 239, 240, 1)")
    

  
   






