from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'appStory6'

urlpatterns = [
	path('', views.landingPage, name="form"),
	path('profile', views.profile, name="profile"),
	path('addstatus', views.coba, name="addstatus"),
	
	# path('delete',views.delete,name='delete')
	# # path('delete/(?P<part_id>[0-9]+)/$', views.function, name='delete_view'),
	# path('<int:delete>/', views.delete, name='delete'),
	 ]