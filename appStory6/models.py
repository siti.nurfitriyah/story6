from django.db import models
from django.utils import timezone
from django import forms



# Create your models here.
class Status(models.Model):
	message = models.CharField(max_length = 300)
	created_at = models.DateTimeField(auto_now_add=True,  null=True, blank=True)

