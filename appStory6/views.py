from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import StatusForm
from .models import Status


def landingPage(request):
  form = StatusForm()
  response = {}
  status = Status.objects.all()
  response['form'] = form
  response['status'] = status
  return render(request, 'form.html', response)

def coba(request):
  form = StatusForm(request.POST or None)
  if (request.method == 'POST' and form.is_valid()):
      status = request.POST.get('status')
      Status.objects.create(message=status)
      print(status)
      return HttpResponseRedirect('/')
  else:
      return HttpResponseRedirect('/')        
    
def profile(request):
    response = {}
    return render(request, 'landingpage.html', response)

# def delete(request, pk):
#   Status.objects.get(message=Status).delete()
#   response = {}
#   return HttpResponseRedirect('/') 


